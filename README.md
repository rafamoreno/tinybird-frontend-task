# Frontend Technical Task

Dashboard that shows Yellow Taxi Trips in NYC during January 2017.

You can check the deployed project [here](https://tinybird-frontend-task.vercel.app)

## How it works

It shows the information with two widgets: one summary of the trip stats and one table for displaying the data in list format. Both widgets are updated when the filters change. You can share the filtered information via url.

This app consumes this [API endpoint](https://api.tinybird.co/endpoint/t_f3b68895534049bf859f38a8e5ebc51a?token=p.eyJ1IjogIjdmOTIwMmMzLWM1ZjctNDU4Ni1hZDUxLTdmYzUzNTRlMTk5YSIsICJpZCI6ICJmZTRkNWFiZS05ZWIyLTRjMjYtYWZiZi0yYTdlMWJlNDQzOWEifQ.P67MfoqTixyasaMGH5RIjCrGc0bUKvBoKMwYjfqQN8c).

## Setup

Install dependencies

```shell
npm install
```

There is a .env file already created with the required env variables, so run the development server

```shell
npm run dev
```

Go to http://localhost:3000 and you will see the app running.

## Design decisions

- This folder structure has been chosen according to functionalities(services, utils, components...) and not according to business domains(trips, passengers, drivers), since being a small-scale application, it would have been overengineering to try a modular approach or something like that.
- All the app renders in the last exposed file main.js. From here, component composition is done as if it were a component-based framework.
- The app.js file is the place where trip widgets are fed with data from the API.
- I simulate the same behavior librarys like history or react-router do, dispatching a popstate event when changing the history state.
- With the popstate event, a state object is passed with the action done by filtering or pagination. This allows us to know which component should re-render.

## Architecture

### Components

UI components and views that would be created in the same way as it were a component-based framework or library.

### Constants

Shared constants used by the whole application to try to avoid to create hardcoded variables and
facilitate maintainability.

### Services

Services that consume the API and transform the data to the format needed by the frontend.

### Utils

Base utilites: api client, dynamic queries, url manipulation, DOM, date formatting, etc...

### Styles

Folder where all the css lives exported in a single file index.css that is imported in the root.

## Testing

Testing coverage is not intended to be 100%. I only tried to test the places of the app
more susceptible to errors.

Running the tests

```shell
npm run test:run
```

Running the tests with coverage

```shell
npm run test:coverage
```

## Follow-ups

- [ ] Loading state for widgets
- [ ] Responsive filters
- [ ] List sorting
- [ ] Improve date range logic
- [ ] E2E tests
