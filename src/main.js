import { renderApp, rerenderApp } from './components/app'
import './styles/index.css'

function renderRoot() {
  window.addEventListener('popstate', rerenderApp)
  renderApp()
}

renderRoot()
