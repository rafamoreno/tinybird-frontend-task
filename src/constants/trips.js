export const rateCodeIds = {
  standardRate: 1,
  jfk: 2,
  newark: 3,
  nassau: 4,
  negotiatedFare: 5,
  groupRide: 6,
}

export const rateCodeIdLabels = {
  [rateCodeIds.standardRate]: 'Standard rate',
  [rateCodeIds.jfk]: 'JFK',
  [rateCodeIds.newark]: 'Newark',
  [rateCodeIds.nassau]: 'Nassau or Westchester',
  [rateCodeIds.negotiatedFare]: 'Negotiated fare',
  [rateCodeIds.groupRide]: 'Group ride',
}

export const rateCodeIdOptions = [
  { value: '', label: '-- Select rate --' },
  ...Object.values(rateCodeIds).map(key => ({
    value: key,
    label: rateCodeIdLabels[key],
  })),
]

export const paymentTypes = {
  creditCard: 1,
  cash: 2,
  noCharge: 3,
  dispute: 4,
  unknown: 5,
  voidedTrip: 6,
}

export const paymentTypeLabels = {
  [paymentTypes.creditCard]: 'Credit card',
  [paymentTypes.cash]: 'Cash',
  [paymentTypes.noCharge]: 'No charge',
  [paymentTypes.dispute]: 'Dispute',
  [paymentTypes.unknown]: 'Unknown',
  [paymentTypes.voidedTrip]: 'Voided trip',
}

export const paymentTypeOptions = [
  { value: '', label: '-- Select payment --' },
  ...Object.values(paymentTypes).map(key => ({
    value: key,
    label: paymentTypeLabels[key],
  })),
]

export const vendorIds = {
  creativeMobile: 1,
  verifone: 2,
}

export const vendorIdLabels = {
  [vendorIds.creativeMobile]: 'Creative Mobile',
  [vendorIds.verifone]: 'VeriFone Inc.',
}

export const vendorIdOptions = [
  { value: '', label: '-- Select vendor --' },
  ...Object.values(vendorIds).map(key => ({
    value: key,
    label: vendorIdLabels[key],
  })),
]

export const filterFields = {
  pickupStartDate: 'tpep_pickup_datetime_start_date',
  pickupEndDate: 'tpep_pickup_datetime_end_date',
  paymentType: 'payment_type',
  rateCodeId: 'ratecodeid',
  vendorId: 'vendorid',
}
