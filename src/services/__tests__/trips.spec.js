import { describe, expect, it } from 'vitest'
import {
  paymentTypeLabels,
  rateCodeIdLabels,
  vendorIdLabels,
} from '../../constants/trips'
import { formatTripStats, renderTripCell } from '../trips'

describe('renderTripCell', () => {
  it('should render same value if field is not controlled', () => {
    const passengerCount = 500
    expect(
      renderTripCell(passengerCount, { name: 'passenger_count', type: 'Int16' })
    ).toBe(passengerCount)
  })
  it('should render a custom value if field is controlled', () => {
    const datetime = '2017-12-23 00:00:00'
    expect(
      renderTripCell(datetime, {
        name: 'tpup_pickup_datetime',
        type: 'DateTime',
      })
    ).toBe('12/23/2017')
    const paymentType = '3'
    expect(renderTripCell(paymentType, { name: 'payment_type' })).toBe(
      paymentTypeLabels[paymentType]
    )
    const rateCodeId = '2'
    expect(renderTripCell(rateCodeId, { name: 'ratecodeid' })).toBe(
      rateCodeIdLabels[rateCodeId]
    )
    const vendorId = '1'
    expect(renderTripCell(vendorId, { name: 'vendorid' })).toBe(
      vendorIdLabels[vendorId]
    )
  })
})

describe('formatTripStats', () => {
  it('should format the data from the api correctly', () => {
    const data = [
      {
        trips: 10,
        passenger_count: 500,
        trip_distance: 100,
        total_amount: 100,
      },
    ]
    const expected = [
      {
        label: 'Trips',
        value: '10',
      },
      {
        label: 'Passengers',
        value: '500',
      },
      {
        label: 'Distance traveled',
        value: '100.00 miles',
      },
      {
        label: 'Charged money',
        value: '$100.00',
      },
    ]
    expect(formatTripStats(data)).toEqual(expected)
  })
})
