import {
  paymentTypeLabels,
  rateCodeIdLabels,
  vendorIdLabels,
} from '../constants/trips'
import { formatDate } from '../utils/dates'
import { formatCurrency, formatDistance, formatNumber } from '../utils/numbers'
import {
  createLimitAndOffset,
  createOrderBy,
  createSum,
  createWhere,
} from '../utils/queries'

export async function getTrips({ api, filters, pagination }) {
  const limitAndOffset = createLimitAndOffset(pagination)
  const where = createWhere(filters)
  const orderBy = createOrderBy({
    field: 'tpep_pickup_datetime',
    direction: 'asc',
  })
  const query = `SELECT * FROM _ ${where} ${orderBy} ${limitAndOffset}`
  const response = await api.get({ q: query })
  const columns = getTripColumns(response.meta)
  return { data: response.data, columns }
}

export function renderTripCell(value, { name, type }) {
  if (type === 'DateTime') return formatDate(value)
  const cellValue = {
    payment_type: paymentTypeLabels[value],
    ratecodeid: rateCodeIdLabels[value],
    vendorid: vendorIdLabels[value],
  }[name]
  return cellValue ?? value
}

function getTripColumns(meta = []) {
  return meta.map(column => ({
    name: column.name,
    renderCell: value => renderTripCell(value, column),
  }))
}

export function formatTripStats(data = []) {
  const statLabels = [
    'Trips',
    'Passengers',
    'Distance traveled',
    'Charged money',
  ]
  return Object.entries(data[0]).map(([key, value], index) => {
    const formatValue = {
      trip_distance: formatDistance,
      total_amount: formatCurrency,
    }[key]
    return {
      label: statLabels[index],
      value: formatValue?.(value) ?? formatNumber(value),
    }
  })
}

export async function getTripStats({ api, filters }) {
  const summableKeys = ['passenger_count', 'trip_distance', 'total_amount']
  const sum = createSum(summableKeys)
  const where = createWhere(filters)
  const q = `SELECT count() trips, ${sum} FROM _ ${where} `
  const result = await api.get({ q })
  return formatTripStats(result.data)
}

export async function getTripData({
  action = 'filter',
  api,
  filters,
  pagination,
}) {
  return Promise.all(
    [
      getTrips({ api, filters, pagination }),
      action === 'filter' && getTripStats({ api, filters }),
    ].filter(Boolean)
  )
}
