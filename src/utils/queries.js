import { filterFields } from '../constants/trips'
import { getISOString } from './dates'

export function createWhere(filters = {}) {
  const conditions = Object.entries(filters).reduce((acc, [key, value]) => {
    const availableFilters = Object.values(filterFields)
    if (!availableFilters.includes(key)) return acc
    const isStartDate = key.includes('start_date')
    const isEndDate = key.includes('end_date')
    const isDate = isStartDate || isEndDate
    const queryKey = isDate ? key.split('_').slice(0, 3).join('_') : key
    const operator = isStartDate ? ' >= ' : isEndDate ? ' <= ' : ' = '
    const queryValue = isDate ? getISOString(value) : value
    return queryValue ? [...acc, `${queryKey}${operator}'${queryValue}'`] : acc
  }, [])
  return conditions.length ? `WHERE ${conditions.join(' AND ')}` : ''
}

export function createSum(summableKeys = []) {
  return summableKeys.map(key => `sum(${key}) ${key}`).join(', ')
}

export function createLimitAndOffset(pagination) {
  const { page = 1, limit = 20 } = pagination ?? {}
  const offset = (page - 1) * limit
  return `LIMIT ${limit} OFFSET ${offset}`
}

export function createOrderBy({ field = '', direction = 'asc' } = {}) {
  if (!field) return ''
  return `ORDER BY ${field} ${direction.toUpperCase()}`
}
