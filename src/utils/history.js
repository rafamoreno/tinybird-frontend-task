export function navigate(url = '/', state = {}) {
  window.history.pushState({}, '', decodeURIComponent(url))
  const popStateEvent = new PopStateEvent('popstate', { state })
  dispatchEvent(popStateEvent)
}

export function setSearchParams(params = {}, state = {}) {
  const searchParams = new URLSearchParams(window.location.search)
  for (const [key, value] of Object.entries(params)) {
    value ? searchParams.set(key, value) : searchParams.delete(key)
  }
  const queryParams = searchParams.toString()
  const url = `${window.location.pathname}${
    queryParams ? `?${queryParams}` : ''
  }`
  navigate(url, state)
}

export function saveUrlToClipboard() {
  navigator.clipboard.writeText(window.location.href)
}
