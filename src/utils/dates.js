export function formatDate(date) {
  return new Intl.DateTimeFormat().format(new Date(date))
}

export function getISOString(date) {
  try {
    return new Date(date).toISOString().split('.000Z')[0].split('T').join(' ')
  } catch (error) {
    return ''
  }
}
