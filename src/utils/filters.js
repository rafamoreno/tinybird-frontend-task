export function getFiltersFromUrl(init) {
  const params = new URLSearchParams(init)
  const paramObj = [...params.entries()].reduce((acc, [key, value]) => {
    acc[key] = value
    return acc
  }, {})
  const { page: pageParam, ...filters } = paramObj
  const page = isNaN(Number(pageParam)) ? 1 : Number(pageParam)
  return { pagination: { page }, filters }
}
