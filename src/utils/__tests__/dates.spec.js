import { describe, expect, it } from 'vitest'
import { getISOString } from '../dates'

describe('getISOString', () => {
  it('should create ISOString date properly', () => {
    expect(getISOString('2022-12-23')).toBe('2022-12-23 00:00:00')
  })

  it('should return empty string if date is not valid', () => {
    expect(getISOString('not-valid-date')).toBe('')
  })
})
