import { describe, expect, it } from 'vitest'
import {
  createLimitAndOffset,
  createOrderBy,
  createSum,
  createWhere,
} from '../queries'

describe('createSum', () => {
  it('should create sum query properly', () => {
    expect(
      createSum(['passenger_count', 'trip_distance', 'total_amount'])
    ).toBe(
      'sum(passenger_count) passenger_count, sum(trip_distance) trip_distance, sum(total_amount) total_amount'
    )
  })
  it('should return an empty string is no data is passed', () => {
    expect(createSum()).toBe('')
  })
})

describe('createWhere', () => {
  it('should create where query properly', () => {
    const filters = {
      tpep_pickup_datetime_start_date: '2017-01-01',
      payment_type: '1',
      ratecodeid: '1',
    }
    expect(createWhere(filters)).toBe(
      "WHERE tpep_pickup_datetime >= '2017-01-01 00:00:00' AND payment_type = '1' AND ratecodeid = '1'"
    )
  })
  it('should return an empty string is no data is passed', () => {
    expect(createWhere()).toBe('')
  })
})

describe('createLimitAndOffset', () => {
  it('should create limit and offset query properly', () => {
    const pagination = {
      page: 3,
      limit: 50,
    }
    expect(createLimitAndOffset(pagination)).toBe('LIMIT 50 OFFSET 100')
  })
  it('should return default pagination if no data is passed', () => {
    expect(createLimitAndOffset()).toBe('LIMIT 20 OFFSET 0')
  })
})

describe('createOrderBy', () => {
  it('should create orderBy query properly', () => {
    const field = 'payment_type'
    expect(createOrderBy({ field, direction: 'desc' })).toBe(
      `ORDER BY ${field} DESC`
    )
  })
  it('should return empty string if no field is passed', () => {
    expect(createOrderBy()).toBe('')
  })
})
