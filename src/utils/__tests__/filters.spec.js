import { describe, expect, it } from 'vitest'
import { getFiltersFromUrl } from '../filters'

describe('getFiltersFromUrl', () => {
  it('should return filters properly', () => {
    expect(getFiltersFromUrl('?ratecodeid=2&payment_type=1&page=2')).toEqual({
      pagination: { page: 2 },
      filters: { ratecodeid: '2', payment_type: '1' },
    })
  })

  it('should return default filters if url is empty', () => {
    expect(getFiltersFromUrl()).toEqual({
      pagination: { page: 1 },
      filters: {},
    })
  })
})
