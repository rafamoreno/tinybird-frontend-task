export const api = {
  get(params) {
    const pipe = 'yellow_tripdata_2017_pipe.json'
    const searchParams = new URLSearchParams()
    Object.entries(params).forEach(([key, value]) => {
      searchParams.append(key, value)
    })
    return fetch(
      `https://api.tinybird.co/v0/pipes/${pipe}?${searchParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${import.meta.env.VITE_TINYBIRD_TOKEN}`,
        },
      }
    )
      .then(response => response.json())
      .catch(error =>
        console.error(`Ooops! Your query failed: ${error.toString()}`)
      )
  },
}
