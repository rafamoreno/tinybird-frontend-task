import { saveUrlToClipboard, setSearchParams } from '../utils/history'
import { renderTripFilters } from './trips/trip-filters'
import { renderHeading } from './ui/heading'

export function renderTopBar(filters) {
  const topBar = document.createElement('header')
  topBar.classList.add('topbar')
  const heading = renderHeading({ text: 'Yellow Trip Data 2017', level: 1 })

  const handleFilter = filters => {
    setSearchParams(filters, { action: 'filter' })
  }

  topBar.append(
    heading,
    renderTripFilters({
      filters,
      onFilter: handleFilter,
      onSave: saveUrlToClipboard,
    })
  )
  return topBar
}
