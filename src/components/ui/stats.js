export function renderStats(stats = []) {
  const descriptionList = document.createElement('dl')
  descriptionList.classList.add('stats')
  const items = stats.map(({ label, value }) => {
    const item = document.createElement('div')
    item.classList.add('stats-item')
    const descriptionTerm = document.createElement('dt')
    descriptionTerm.innerText = label
    const descriptionDetails = document.createElement('dd')
    descriptionDetails.innerText = value
    item.append(descriptionTerm, descriptionDetails)
    return item
  })
  descriptionList.append(...items)
  return descriptionList
}
