import { renderButton } from './button'

function renderPagination({ pagination, onSelectPage }) {
  const { page = 1 } = pagination ?? {}
  const prevButton = renderButton({
    text: 'Previous',
    onClick: () => onSelectPage(page - 1),
    disabled: page === 1,
  })
  const nextButton = renderButton({
    text: 'Next',
    onClick: () => onSelectPage(page + 1),
  })
  const container = document.createElement('div')
  container.classList.add('pagination')
  container.append(prevButton, nextButton)
  return container
}

function renderHeader(columns) {
  const thead = document.createElement('thead')
  const headRow = document.createElement('tr')
  const headerCells = columns.map(({ name }) => {
    const th = document.createElement('th')
    th.innerHTML = name
    return th
  })
  headRow.append(...headerCells)
  thead.appendChild(headRow)
  return thead
}

function renderBody({ data, columns }) {
  const tbody = document.createElement('tbody')
  const rows = data.map(item => {
    const tr = document.createElement('tr')
    columns.forEach(({ name, renderCell }) => {
      const value = item[name]
      const td = document.createElement('td')
      td.innerHTML = renderCell(value)
      tr.appendChild(td)
      return td
    })
    return tr
  })
  tbody.append(...rows)
  return tbody
}

export function renderTable({
  data = [],
  columns = [],
  onSelectPage = () => {},
  pagination = {},
} = {}) {
  const container = document.createElement('div')
  container.classList.add('table')
  const tableContainer = document.createElement('div')
  tableContainer.classList.add('table-container')
  const table = document.createElement('table')
  const thead = renderHeader(columns)
  table.appendChild(thead)
  const tbody = renderBody({ data, columns })
  table.appendChild(tbody)
  tableContainer.appendChild(table)
  container.append(
    tableContainer,
    renderPagination({ pagination, onSelectPage })
  )
  return container
}
