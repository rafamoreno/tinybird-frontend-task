import { renderInput } from './input'

export function renderDateRange({ startDateInputProps, endDateInputProps }) {
  const container = document.createElement('div')
  container.classList.add('date-range')
  const startDateInput = renderInput({ ...startDateInputProps, type: 'date' })
  const endDateInput = renderInput({ ...endDateInputProps, type: 'date' })
  container.append(startDateInput, endDateInput)
  return container
}
