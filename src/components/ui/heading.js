export function renderHeading({ text = '', level = 2 } = {}) {
  const heading = document.createElement(`h${level}`)
  heading.innerText = text
  return heading
}
