export function renderInput({
  type = 'text',
  name = '',
  id = '',
  min = '',
  max = '',
  onChange = () => {},
  defaultValue = '',
  label: labelText = '',
} = {}) {
  const inputContainer = document.createElement('div')
  const label = document.createElement('label')
  label.innerHTML = labelText
  label.htmlFor = id
  const input = document.createElement('input')
  input.id = id
  input.type = type
  input.name = name
  input.min = min
  input.max = max
  input.value = defaultValue
  input.addEventListener('change', onChange)
  inputContainer.append(label, input)
  return inputContainer
}
