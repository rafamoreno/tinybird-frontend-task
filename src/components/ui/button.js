export function renderButton({
  text = 'Click me',
  type = 'button',
  onClick = () => {},
  disabled = false,
  variant = 'secondary',
}) {
  const button = document.createElement('button')
  button.innerText = text
  button.type = type
  button.disabled = disabled
  button.addEventListener('click', onClick)
  button.classList.add(variant)
  return button
}
