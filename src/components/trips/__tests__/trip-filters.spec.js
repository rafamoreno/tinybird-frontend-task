import { fireEvent, getByLabelText, getByRole } from '@testing-library/dom'
import { describe, expect, it, vi } from 'vitest'
import { renderTripFilters } from '../trip-filters'

describe('TripFilters', () => {
  it('should set default filters properly', () => {
    const filters = {
      tpep_pickup_datetime_start_date: '2017-01-01',
      tpep_pickup_datetime_end_date: '2017-01-05',
      payment_type: '5',
      ratecodeid: '2',
      vendorid: '1',
    }
    const container = renderTripFilters({ filters })
    const startDateInput = getByLabelText(container, /pickup start date/i)
    const endDateInput = getByLabelText(container, /pickup end date/i)
    const vendorSelect = getByLabelText(container, /vendor/i)
    const rateCodeSelect = getByLabelText(container, /rate code/i)
    const paymentTypeSelect = getByLabelText(container, /payment type/i)
    expect(startDateInput.value).toBe(filters.tpep_pickup_datetime_start_date)
    expect(endDateInput.value).toBe(filters.tpep_pickup_datetime_end_date)
    expect(vendorSelect.value).toBe(filters.vendorid)
    expect(rateCodeSelect.value).toBe(filters.ratecodeid)
    expect(paymentTypeSelect.value).toBe(filters.payment_type)
  })
  it('should change filters properly', () => {
    const onFilter = vi.fn()
    const onSave = vi.fn()
    const container = renderTripFilters({ onFilter, onSave })
    const filters = {
      tpep_pickup_datetime_start_date: '2017-01-01',
      tpep_pickup_datetime_end_date: '2017-01-05',
      payment_type: '5',
      ratecodeid: '2',
      vendorid: '1',
    }
    const startDateInput = getByLabelText(container, /pickup start date/i)
    fireEvent.focus(startDateInput)
    fireEvent.change(startDateInput, {
      target: { value: filters.tpep_pickup_datetime_start_date },
    })
    const endDateInput = getByLabelText(container, /pickup end date/i)
    fireEvent.focus(endDateInput)
    fireEvent.change(endDateInput, {
      target: { value: filters.tpep_pickup_datetime_end_date },
    })
    const vendorSelect = getByLabelText(container, /vendor/i)
    fireEvent.focus(vendorSelect)
    fireEvent.change(vendorSelect, { target: { value: filters.vendorid } })
    const rateCodeSelect = getByLabelText(container, /rate code/i)
    fireEvent.focus(rateCodeSelect)
    fireEvent.change(rateCodeSelect, {
      target: { value: filters.ratecodeid },
    })
    const paymentTypeSelect = getByLabelText(container, /payment type/i)
    fireEvent.focus(paymentTypeSelect)
    fireEvent.change(paymentTypeSelect, {
      target: { value: filters.payment_type },
    })
    expect(onFilter).toHaveBeenCalledTimes(5)
    expect(onFilter).toHaveBeenCalledWith(filters)
    const saveButton = getByRole(container, /button/i)
    expect(saveButton.innerText).toBe('Save search')
    fireEvent.click(saveButton)
    expect(saveButton.innerText).toBe('Saved!')
    expect(onSave).toHaveBeenCalledOnce()
  })
})
