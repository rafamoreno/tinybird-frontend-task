import {
  filterFields,
  paymentTypeOptions,
  rateCodeIdOptions,
  vendorIdOptions,
} from '../../constants/trips'
import { renderButton } from '../ui/button'
import { renderDateRange } from '../ui/date-range'
import { renderSelect } from '../ui/select'

export function renderTripFilters({
  filters = {
    tpep_pickup_datetime: undefined,
    payment_type: undefined,
    ratecodeid: undefined,
    vendorid: undefined,
  },
  onFilter = () => {},
  onSave = () => {},
} = {}) {
  const filterContainer = document.createElement('div')
  filterContainer.classList.add('trip-filters')

  const handleChange = event => {
    const { value, name } = event.target
    filters[name] = value
    onFilter(filters)
  }

  // added this range because the dataset is from january 2017
  const min = '2017-01-01'
  const max = '2017-01-31'

  const { paymentType, pickupEndDate, pickupStartDate, rateCodeId, vendorId } =
    filterFields

  const dateInput = renderDateRange({
    startDateInputProps: {
      id: pickupStartDate,
      name: pickupStartDate,
      min,
      max,
      onChange: handleChange,
      label: 'Pickup start date',
      defaultValue: filters[pickupStartDate],
    },
    endDateInputProps: {
      id: pickupEndDate,
      name: pickupEndDate,
      min,
      max,
      onChange: handleChange,
      label: 'Pickup end date',
      defaultValue: filters[pickupEndDate],
    },
  })

  const vendorSelect = renderSelect({
    id: vendorId,
    name: vendorId,
    options: vendorIdOptions,
    onChange: handleChange,
    defaultValue: filters[vendorId],
    label: 'Vendor',
  })
  const rateCodeSelect = renderSelect({
    id: rateCodeId,
    name: rateCodeId,
    options: rateCodeIdOptions,
    onChange: handleChange,
    defaultValue: filters[rateCodeId],
    label: 'Rate code',
  })
  const paymentTypeSelect = renderSelect({
    id: paymentType,
    name: paymentType,
    options: paymentTypeOptions,
    onChange: handleChange,
    defaultValue: filters[paymentType],
    label: 'Payment type',
  })

  const handleSave = event => {
    event.target.innerText = 'Saved!'
    setTimeout(() => {
      event.target.innerText = 'Save search'
    }, 1000)
    onSave()
  }

  const saveButton = renderButton({
    text: 'Save search',
    onClick: handleSave,
    variant: 'primary',
  })

  filterContainer.append(
    dateInput,
    vendorSelect,
    rateCodeSelect,
    paymentTypeSelect,
    saveButton
  )
  return filterContainer
}
