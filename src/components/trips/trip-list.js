import { setSearchParams } from '../../utils/history'
import { renderHeading } from '../ui/heading'
import { renderTable } from '../ui/table'

export function renderTripList({
  data = [],
  columns = [],
  pagination = {},
} = {}) {
  const widget = document.createElement('div')
  widget.classList.add('trip-list')
  const title = renderHeading({ text: 'Trips', level: 2 })

  const handleSelectPage = page =>
    setSearchParams({ page }, { action: 'pagination' })

  widget.append(
    title,
    renderTable({ data, columns, pagination, onSelectPage: handleSelectPage })
  )
  return widget
}

export function rerenderTripList(data) {
  const widget = document.querySelector('.trip-list')
  if (!widget) return
  widget.remove()
  return renderTripList(data)
}
