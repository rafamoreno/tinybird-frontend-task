import { renderHeading } from '../ui/heading'
import { renderStats } from '../ui/stats'

export function renderTripStats(stats = []) {
  const widget = document.createElement('div')
  widget.classList.add('trip-stats')
  const title = renderHeading({ text: 'Summary', level: 2 })
  widget.append(title, renderStats(stats))
  return widget
}

export function rerenderTripStats(stats) {
  const widget = document.querySelector('.trip-stats')
  if (!widget) return
  widget.remove()
  return renderTripStats(stats)
}
