import { getTripData } from '../services/trips'
import { api } from '../utils/api'
import { appendRoot } from '../utils/dom'
import { getFiltersFromUrl } from '../utils/filters'
import { renderTopBar } from './topbar'
import { renderTripList, rerenderTripList } from './trips/trip-list'
import { renderTripStats, rerenderTripStats } from './trips/trip-stats'

export function renderApp() {
  const { filters, pagination } = getFiltersFromUrl(window.location.search)
  getTripData({ api, filters, pagination }).then(([trips, stats]) => {
    appendRoot(
      renderTopBar(filters),
      renderTripStats(stats),
      renderTripList({ ...trips, pagination })
    )
  })
}

export function rerenderApp({ state }) {
  const { action } = state
  const { filters, pagination } = getFiltersFromUrl(window.location.search)
  getTripData({ api, filters, pagination, action }).then(([trips, stats]) => {
    const children = [
      action === 'filter' && rerenderTripStats(stats),
      rerenderTripList({ ...trips, pagination }),
    ].filter(Boolean)
    appendRoot(...children)
  })
}
